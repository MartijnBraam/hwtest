from setuptools import setup

setup(
    name='hwtest',
    version='0.7.0',
    packages=['hwtest'],
    url='https://gitlab.com/MartijnBraam/hwtest',
    license='MIT',
    author='Martijn Braam',
    author_email='martijn@brixit.nl',
    description='Tool for testing hardware on linux phone',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5'
    ],
    entry_points={
        'console_scripts': [
            'hwtest=hwtest.__main__:main'
        ]
    }
)
